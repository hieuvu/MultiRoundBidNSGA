package com.paduvi.util;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import com.google.gson.FieldNamingPolicy;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.google.gson.JsonIOException;
import com.google.gson.JsonSyntaxException;
import com.paduvi.app.App;

public class Utils {
	public static <T> T getDataFromJsonFile(String filesource, Class<T> cl) {
		GsonBuilder gsonBuilder = new GsonBuilder();
		gsonBuilder.setFieldNamingPolicy(FieldNamingPolicy.LOWER_CASE_WITH_UNDERSCORES);
		Gson gson = gsonBuilder.setDateFormat("dd/MM/yyyy").create();
		File file = new File(App.class.getResource(filesource).getPath());

		try {
			T obj = gson.fromJson(new FileReader(file), cl);
			return obj;
		} catch (JsonSyntaxException | JsonIOException | FileNotFoundException e) {
			e.printStackTrace();
			return null;
		}
	}
	
	public static int daysBetween(Date from, Date to) {
		return Math.abs((int)((to.getTime() - from.getTime()) / (1000 * 60 * 60 * 24)));
	}
	
	public static Date addDay(Date from, int days) {
		Calendar cal = Calendar.getInstance();
		cal.setTime(from);
		cal.add(Calendar.DATE, days);
		return cal.getTime();
	}
	
	public static String dateString(Date d) {
		return new SimpleDateFormat("dd/MM/yyyy").format(d);
	}
}
