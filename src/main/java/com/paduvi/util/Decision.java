package com.paduvi.util;

import java.util.Arrays;
import java.util.function.Function;
import java.util.function.UnaryOperator;
import java.util.stream.DoubleStream;

public class Decision {
	
	public static final int TOPSIS_SMEAN = 0, TOPSIS_SMAX = 1, TOPSIS_SMIN = 2;
	
	public static double[][] topsis(double[][] X, double[] W) {
		double[][] R1 = mapRow(transpose(X), v -> vectorNorm(v));
		return topsis(X, W, Raw -> transpose(R1));
	}
	
	public static double[][] topsis(double[][] X, double[] W, UnaryOperator<double[][]> normalizeFunc) {
		double[][] R = normalizeFunc.apply(X);
		
		double[][] V = mapRow(R, v -> dotEle(v, W));
		double[][] V1 = transpose(V);
		double[] VMax = max(V1);
		double[] VMin = min(V1);
		
		double[][] rs = new double[3][];
		double[] SMax = reduceRow(V, v -> euclid(v, VMax));
		double[] SMin = reduceRow(V, v -> euclid(v, VMin));
		double[] SMean = new double[SMin.length];
		for(int i = 0; i < SMean.length; i++) {
			SMean[i] = SMin[i] / (SMin[i] + SMax[i]);
		}
		rs[TOPSIS_SMAX] = SMax;
		rs[TOPSIS_SMIN] = SMin;
		rs[TOPSIS_SMEAN] = SMean;
		return rs;
	}
	
	public static double[] maxNorm(double[] v) {
		double max = max(v);
		return DoubleStream.of(v).map(x -> x - max).toArray();
	}
	
	public static double[] vectorNorm(double[] v) {
		double totalSquare = DoubleStream.of(v).reduce(0, (x, e) -> x + e*e);
		double totalSqrt = Math.sqrt(totalSquare);
		return DoubleStream.of(v).map(x -> x / totalSqrt).toArray();
	}
	
	public static double[][] transpose(double[][] matrix) {
		int rows = matrix.length;
		int cols = matrix[0].length;
		double[][] rs = new double[cols][rows];
		for (int r = 0; r <rows; r++) {
			for(int c = 0; c < cols; c++) {
				rs[c][r] = matrix[r][c];
			}
		}
		return rs;
	}
	
	public static double[] dotEle(double[] v, double[] w) {
		double[] rs = new double[v.length];
		for (int i = 0; i < rs.length; i++) {
			rs[i] = v[i] * w[i];
		}
		return rs;
	}
	
	public static double euclid(double[] v, double[] u) {
		double[] totalSquare = new double[v.length];
		for (int i = 0; i < totalSquare.length; i++) {
			totalSquare[i] = (v[i] - u[i]) * (v[i] - u[i]);
		}
		return Math.sqrt(DoubleStream.of(totalSquare).sum());
	}
	
	
	public static double[][] mapRow(double[][] m, UnaryOperator<double[]> f) {
		double[][] rs = new double[m.length][];
		for (int i = 0; i < rs.length; i++) {
			rs[i] = f.apply(m[i]);
		}
		return rs;
	}
	
	public static double[] reduceRow(double[][] m, Function<double[], Double> f) {
		double[] rs = new double[m.length];
		for (int i = 0; i < rs.length; i++) {
			rs[i] = f.apply(m[i]);
		}
		return rs;
	}
	
	public static int maxIndex(double[] v) {
		int rs = 0;
		for(int i = 0; i < v.length; i++) {
			if (v[i] > v[rs]) {
				rs = i;
			}
		}
		return rs;
	}
	
	public static double max(double[] v) {
		return DoubleStream.of(v).max().getAsDouble();
	}
	
	public static double[] max(double[][] m) {
		double[] rs = new double[m.length];
		for(int i = 0; i < rs.length; i++) {
			rs[i] = max(m[i]);
		}
		return rs;
	}
	
	public static double min(double[] v) {
		return DoubleStream.of(v).min().getAsDouble();
	}
	
	public static double[] min(double[][] m) {
		double[] rs = new double[m.length];
		for(int i = 0; i < rs.length; i++) {
			rs[i] = min(m[i]);
		}
		return rs;
	}
	
	public static String toString(double[][] m) {
		String[] rs = new String[m.length];
		for (int i = 0; i < rs.length; i++) {
			rs[i] = Arrays.toString(m[i]);
		}
		return '[' + String.join(",\n", rs) + ']';
	}
	
	public static String toString(double[] v) {
		return Arrays.toString(v);
	}
}
