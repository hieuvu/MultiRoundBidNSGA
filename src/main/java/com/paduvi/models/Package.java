package com.paduvi.models;

import java.sql.Date;
import java.text.SimpleDateFormat;

public class Package {

	private int packageId;
	private long estimatedCost;
	private ProductDemand products[];
	private Timeline timeline;
	private int[] joinedContractors;
	
	public long getEstimatedCost() {
		return estimatedCost;
	}

	public void setEstimatedCost(long estimatedCost) {
		this.estimatedCost = estimatedCost;
	}

	public Timeline getTimeline() {
		return timeline;
	}
	
	public Date getFromDate() {
		return timeline.getFrom();
	}
	
	public Date getToDate() {
		return timeline.getTo();
	}

	public void setTimeline(Timeline timeline) {
		this.timeline = timeline;
	}

	public int getPackageId() {
		return packageId;
	}

	public ProductDemand[] getProducts() {
		return products;
	}

	public void setProducts(ProductDemand[] products) {
		this.products = products;
	}

	public int[] getJoinedContractors() {
		return joinedContractors;
	}

	public void setJoinedContractors(int[] joinedContractors) {
		this.joinedContractors = joinedContractors;
	}

	public void setPackageId(int packageId) {
		this.packageId = packageId;
	}
	
	public static class Timeline {
		private Date from;
		private Date to;
		public Date getFrom() {
			return from;
		}
		public void setFrom(Date from) {
			this.from = from;
		}
		public Date getTo() {
			return to;
		}
		public void setTo(Date to) {
			this.to = to;
		}
		
		@Override
		public String toString() {
			SimpleDateFormat formatter = new SimpleDateFormat("dd/MM/yyyy");
			return String.format("{%s, %s}", formatter.format(from), formatter.format(to));
		}
	}

	public static class ProductDemand {
		private int productId;
		private int quantity;
		public int getProductId() {
			return productId;
		}
		public void setProductId(int productId) {
			this.productId = productId;
		}
		public int getQuantity() {
			return quantity;
		}
		public void setQuantity(int quantity) {
			this.quantity = quantity;
		}
	}
}



