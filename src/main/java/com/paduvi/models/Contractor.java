package com.paduvi.models;

import java.util.Date;
import java.util.List;

public class Contractor {
	private int contractorId;
	private String description;
	private double relationship;
	private double quality;
	private List<ProductExchange> products;

	public int getContractorId() {
		return contractorId;
	}

	public void setContractorId(int contractorId) {
		this.contractorId = contractorId;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public double getRelationship() {
		return relationship;
	}

	public void setRelationship(double relationship) {
		this.relationship = relationship;
	}

	public double getQuality() {
		return quality;
	}

	public void setQuality(double quality) {
		this.quality = quality;
	}

	public List<ProductExchange> getProducts() {
		return products;
	}

	public ProductExchange getProductExchange(int productId) {
		return products.get(productId);
	}

	public void setProducts(List<ProductExchange> products) {
		this.products = products;
	}
	
	public static class ProductExchange {
		private int productId;
		private long buyPrice;
		private long sellPrice;
		private List<Discount> discounts;

		public int getProductId() {
			return productId;
		}

		public void setProductId(int productId) {
			this.productId = productId;
		}

		public long getBuyPrice() {
			return buyPrice;
		}

		public void setBuyPrice(long buyPrice) {
			this.buyPrice = buyPrice;
		}

		public long getSellPrice() {
			return sellPrice;
		}

		public void setSellPrice(long sellPrice) {
			this.sellPrice = sellPrice;
		}

		public List<Discount> getDiscounts() {
			return discounts;
		}

		public void setDiscounts(List<Discount> discounts) {
			this.discounts = discounts;
		}
		
		public double getDiscount(Date date) {
			for(Discount d: discounts) {
				if(date.equals(d.getFrom()) || date.equals(d.getTo()) || (date.before(d.getTo()) && date.after(d.getFrom()))) {
					return d.getRate();
				}
			}
			return 0;
		}
	}

	public static class Discount {
		private Date from;
		private Date to;
		private double rate;

		public Date getFrom() {
			return from;
		}

		public void setFrom(Date from) {
			this.from = from;
		}

		public Date getTo() {
			return to;
		}

		public void setTo(Date to) {
			this.to = to;
		}

		public double getRate() {
			return rate;
		}

		public void setRate(double rate) {
			this.rate = rate;
		}
	}
}


