package com.paduvi.models;

import java.util.Date;
import java.util.List;

public class Project {

	private int projectId;
	private double inflationRate;
	private List<Package> packages;
	private Date startDate;
	private List<Contractor> contractors;
	private List<Product> product;

	public List<Product> getProduct() {
		return product;
	}

	public void setProduct(List<Product> product) {
		this.product = product;
	}

	public List<Contractor> getContractors() {
		return contractors;
	}
	
	public Contractor getContractor(int index) {
		return contractors.get(index);
	}

	public void setContractors(List<Contractor> contractors) {
		this.contractors = contractors;
	}

	public List<Package> getPackages() {
		return packages;
	}
	
	public Package getPackage(int index) {
		return packages.get(index);
	}

	public void setPackages(List<Package> packages) {
		this.packages = packages;
	}

	public double getInflationRate() {
		return inflationRate;
	}

	public void setInflationRate(double inflationRate) {
		this.inflationRate = inflationRate;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}
	
	public int getProjectId() {
		return projectId;
	}

	public void setProjectId(int projectId) {
		this.projectId = projectId;
	}

	public void stats() {
		System.out.println("=============");
		System.out.println("Project id: " + projectId);
		System.out.println("nContractors: " + this.getContractors().size());
		System.out.println("nPackages: " + this.getPackages().size());
		System.out.println("infra rate: " + this.getInflationRate());
		System.out.println("Esitmated Cost: " + packages.stream().mapToDouble(pkg -> pkg.getEstimatedCost()).sum());
	}

}
