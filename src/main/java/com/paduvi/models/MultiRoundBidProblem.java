package com.paduvi.models;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.moeaframework.core.Problem;
import org.moeaframework.core.Solution;
import org.moeaframework.core.variable.EncodingUtils;
import org.moeaframework.util.Vector;

import com.paduvi.util.Utils;

public class MultiRoundBidProblem implements Problem {

	private Project project;
	private int nContractor;
	private int nPackage;
	private Date startDate;

	public MultiRoundBidProblem(Project project) {
		this.project = project;
		this.nContractor = project.getContractors().size();
		this.nPackage = project.getPackages().size();
		this.startDate = project.getStartDate();
	}

	@Override
	public String getName() {
		return "MultiRoundBidProblem";
	}

	@Override
	public int getNumberOfVariables() {
		return nPackage * 2;
	}

	@Override
	public int getNumberOfObjectives() {
		return nContractor + 1 + 1 + 1;
	}

	@Override
	public int getNumberOfConstraints() {
		return 0;
	}

	@Override
	public Solution newSolution() {
		Solution s = new Solution(getNumberOfVariables(), getNumberOfObjectives());

		// map contractor
		for (int pkgIndex = 0; pkgIndex < nPackage; pkgIndex++) {
			Package pkg = project.getPackage(pkgIndex);
			s.setVariable(pkgIndex, EncodingUtils.newInt(0, pkg.getJoinedContractors().length - 1));
		}

		// map thoi gian to chuc dau thau
		for (int pkgIndex = 0; pkgIndex < nPackage; pkgIndex++) {
			Package pkg = project.getPackage(pkgIndex);
			int days = Utils.daysBetween(pkg.getFromDate(), pkg.getToDate());
			s.setVariable(nPackage + pkgIndex, EncodingUtils.newInt(0, days - 1));
		}

		return s;
	}

	@Override
	public void evaluate(Solution solution) {
		double[] f = new double[getNumberOfObjectives()];

		for (int cIndex = 0; cIndex < nContractor; cIndex++) {
			f[cIndex] = -calcContractorProfit(cIndex, solution);
		}

		f[nContractor] = -calcOwnerProfit(solution);

		f[nContractor + 1] = -calcQualityProfit(solution);
		f[nContractor + 1 + 1] = calcRelationProfit(f);

		solution.setObjectives(f);
	}

	private double calcOwnerProfit(Solution solution) {
		double r = project.getInflationRate();

		double profit = 0;
		int[] pkgMap = getPkgMap(solution);
		int[] pkgTimeOffset = getPkgTimeOffset(solution);
		for (int pkgIndex = 0; pkgIndex < nPackage; pkgIndex++) {
			Package pkg = project.getPackage(pkgIndex);
			Contractor contractor = project.getContractor(pkgMap[pkgIndex]);
			Date holdDate = Utils.addDay(pkg.getFromDate(), pkgTimeOffset[pkgIndex]);

			double packageCost = 0;
			for (Package.ProductDemand prod : pkg.getProducts()) {
				Contractor.ProductExchange exChange = contractor.getProductExchange(prod.getProductId());
				double discountRate = exChange.getDiscount(holdDate);
				packageCost += exChange.getSellPrice() * prod.getQuantity() * (1 - discountRate);
			}

			profit += (pkg.getEstimatedCost() - packageCost) * Math.exp(r * Utils.daysBetween(startDate, holdDate));
		}
		return profit;
	}

	private double calcContractorProfit(int cIndex, Solution solution) {
		double r = project.getInflationRate();

		double profit = 0;
		int[] pkgMap = getPkgMap(solution);
		int[] pkgTimeOffset = getPkgTimeOffset(solution);

		for (int pkgIndex = 0; pkgIndex < nPackage; pkgIndex++) {
			if (pkgMap[pkgIndex] == cIndex) {
				Package pkg = project.getPackage(pkgIndex);
				Contractor contractor = project.getContractor(pkgMap[pkgIndex]);
				Date holdDate = Utils.addDay(pkg.getFromDate(), pkgTimeOffset[pkgIndex]);

				double packageCost = 0;
				for (Package.ProductDemand prod : pkg.getProducts()) {
					Contractor.ProductExchange exChange = contractor.getProductExchange(prod.getProductId());
					double discountRate = exChange.getDiscount(holdDate);
					packageCost += (exChange.getSellPrice() * (1 - discountRate) - exChange.getBuyPrice())
							* prod.getQuantity();
				}
				profit += packageCost * Math.exp(r * Utils.daysBetween(startDate, holdDate));
			}
		}
		return profit;
	}

	private double calcQualityProfit(Solution solution) {
		int[] pkgMap = getPkgMap(solution);
		double profit = 0;
		for (int pkgIndex = 0; pkgIndex < nPackage; pkgIndex++) {
			Contractor contractor = project.getContractor(pkgMap[pkgIndex]);
			profit += contractor.getQuality();
		}
		return profit;
	}

	private double calcRelationProfit(double[] contractorProfit) {
		double profit = 0;
		for (int i = 0; i < nContractor - 1; i++) {
			for (int j = i + 1; j < nContractor; j++) {
				Contractor ci = project.getContractor(i);
				Contractor cj = project.getContractor(i);
				profit += Math
						.abs(ci.getRelationship() * contractorProfit[i] - cj.getRelationship() * contractorProfit[j]);
			}
		}
		return profit;
	}

	public int[] getPkgMap(Solution solution) {
		int[] pkgMap = new int[nPackage];
		for (int pkgIndex = 0; pkgIndex < nPackage; pkgIndex++) {
			Package pkg = project.getPackage(pkgIndex);
			int[] joinedContractor = pkg.getJoinedContractors();
			pkgMap[pkgIndex] = joinedContractor[EncodingUtils.getInt(solution.getVariable(pkgIndex))];
		}
		return pkgMap;
	}

	public int[] getPkgTimeOffset(Solution solution) {
		int[] pkgTimeOffset = new int[nPackage];
		for (int pkgIndex = 0; pkgIndex < nPackage; pkgIndex++) {
			pkgTimeOffset[pkgIndex] = EncodingUtils.getInt(solution.getVariable(nPackage + pkgIndex));
		}
		return pkgTimeOffset;
	}

	public void solutionStats(Solution sol) {
		double[] objectives = Vector.negate(sol.getObjectives());
		System.out.println("Objectives: " + Arrays.toString(objectives));
		List<String> packageTimeline = new ArrayList<>();
		List<String> packgeTime = new ArrayList<>();
		List<String> joinedContractor = new ArrayList<>();

		int[] pkgOffset = getPkgTimeOffset(sol);
		for (int pkgIndex = 0; pkgIndex < nPackage; pkgIndex++) {
			Package pkg = project.getPackage(pkgIndex);
			packgeTime.add(Utils.dateString(Utils.addDay(pkg.getFromDate(), pkgOffset[pkgIndex])));
			packageTimeline.add(pkg.getTimeline().toString());
			joinedContractor.add(Arrays.toString(pkg.getJoinedContractors()));
		}
		System.out.println("Package Timeline: " + packageTimeline);
		System.out.println("Package Time Hold: " + packgeTime);
		System.out.println("Contractor Map: " + Arrays.toString(getPkgMap(sol)));
		System.out.println("Joined Contractor: " + joinedContractor);
	}
	
	public void showResult(Solution sol) {
		int[] pkgOffset = getPkgTimeOffset(sol);
		int[] pkgMap = getPkgMap(sol);
		
		for(int i = 0; i < nPackage; i++) {
			Package pkg = project.getPackage(i);
			System.out.println("Goi thau " + i + ": ");
			System.out.println("---Nha thau:" + pkgMap[i]);
			System.out.println("---Thoi gian to chuc: " + Utils.dateString(Utils.addDay(pkg.getFromDate(), pkgOffset[i])));
		}
	}
	
	public int getnContractor() {
		return nContractor;
	}

	public int getnPackage() {
		return nPackage;
	}

	@Override
	public void close() {
	}

}
