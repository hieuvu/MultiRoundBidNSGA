package com.paduvi.app;

import java.util.Arrays;

import org.moeaframework.Executor;
import org.moeaframework.core.NondominatedPopulation;
import org.moeaframework.core.Solution;
import org.moeaframework.util.Vector;

import com.paduvi.models.MultiRoundBidProblem;
import com.paduvi.models.Project;
import com.paduvi.util.Decision;
import com.paduvi.util.Utils;

public class App {
	public static void main(String[] args) throws Exception {
		multiRoundBid();
	}

	public static void multiRoundBid() {
		Project prj = Utils.getDataFromJsonFile("/data_sample/data.json", Project.class);
		MultiRoundBidProblem problem = new MultiRoundBidProblem(prj);
		long startTime = System.currentTimeMillis();
		int maxIter = 1000;
		NondominatedPopulation result = new Executor()
				.withProblem(problem)
				.withAlgorithm("NSGAII")
				.withMaxEvaluations(maxIter)
				.run();
		prj.stats();
		System.out.println("====");
		System.out.println("Max iter: " + maxIter);
		System.out.println("Time run: " + (System.currentTimeMillis() - startTime) + "ms");
		System.out.println("N solution: " + result.size());
		Solution[] sols = new Solution[result.size()];
		for (int i = 0; i < sols.length; i++) {
			sols[i] = result.get(i);
		}

		double[][] X = buildXFromSolutions(sols);
		double[] W = new double[problem.getnContractor() + 1 + 1 + 1];
		Arrays.fill(W, 1);
		double[][] topsisResult = Decision.topsis(X, W);
		int topsisBestIndex = Decision.maxIndex(topsisResult[Decision.TOPSIS_SMEAN]);
		Solution best = sols[topsisBestIndex];
		
		System.out.println("====Best");
		problem.solutionStats(best);
		
		System.out.println("=======");
		System.out.println("Formatted");
		prj.stats();
		System.out.println("Tham so muc tieu: " + Arrays.toString(W));
		double[] f = Vector.negate(sols[topsisBestIndex].getObjectives());
		System.out.println("Can bang Nash: ");
		System.out.println("Profit Chu dau tu: " + f[problem.getnContractor()]);
		for(int i = 0; i < problem.getnContractor(); i++) {
			System.out.println("Profit Nha thau " + i + ": "+ f[i]);
		}
		problem.showResult(best);
	}

	public static double[][] buildXFromSolutions(Solution[] sols) {
		double[][] rs = new double[sols.length][];
		for (int i = 0; i < rs.length; i++) {
			Solution sol = sols[i];
			double[] f = Vector.negate(sol.getObjectives());
			f[f.length - 1] *= -1;
			rs[i] = f;
		}
		return rs;
	}

}
